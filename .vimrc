let mapleader =","

" Vim-plug
call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'potatoesmaster/i3-vim-syntax'
Plug 'scrooloose/nerdtree'
Plug 'vimwiki/vimwiki'
Plug 'suan/vim-instant-markdown'
call plug#end()

" set color scheme
syntax enable
set background=light
"colorscheme solarized
set t_Co=16  

" Set Gobal y and p clipboard
"set clipboard+=unnamedplus

" set solarized dark theme for air-line
"let g:airline_solarized_bg='dark'

" Set python
"let g:python3_host_prog = '$HOME/.local/lib/python3.7'
" Enable autocompletion:
set wildmode=longest,list,full
" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Split window below and to the right
set splitbelow splitright

" Necesary  for lots of cool vim things
set nocompatible

" This shows what you are typing as a command.  I love this!
set showcmd

" Folding Stuffs
set foldmethod=marker

" Needed for Syntax Highlighting and stuff
filetype on
filetype plugin on
set grepprg=grep\ -nH\ $*

" Who doesn't like autoindent?
set autoindent
" Line Numbers PWN!
set number
set relativenumber

" Ignoring case is a fun trick
set ignorecase

" And so is Artificial Intellegence!
set smartcase

" Spaces are better than a tab character
set expandtab
set smarttab

" Spell check
" Who wants an 8 character tab?  Not me!
set shiftwidth=4
set softtabstop=4

" mouse cusor
set mouse=a

" NerDTree set and key.
"" autocmd vimenter * NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
map <C-n> :NERDTreeToggle<CR>

" vimwiki with markdown support
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
" helppage -> :h vimwiki-syntax 

" vim-instant-markdown - Instant Markdown previews from Vim
" https://github.com/suan/vim-instant-markdown
let g:instant_markdown_autostart = 0	" disable autostart
map <leader>md :InstantMarkdownPreview<CR>
" Compile document, be it groff/LaTeX/markdown/etc.
map <leader>c :w! \| !compiler <c-r>%<CR>

" Open corresponding .pdf/.html or preview
map <leader>p :!opout <c-r>%<CR><CR>

" clipboard require gvim installed
vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+

" autocmd
map <F2> <Esc>:w<CR>:!filename.py<CR>

" mpv <F8> 
map <F8> :exec '!nohup mpv ' . shellescape(getline('.'), 1) . ' >/dev/null 2>&1&'<CR><CR>

